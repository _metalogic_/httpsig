module bitbucket.org/_metalogic_/httpsig

require (
	bitbucket.org/_metalogic_/log v1.4.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	golang.org/x/sys v0.0.0-20200926100807-9d91bd62050c // indirect
)

go 1.18
